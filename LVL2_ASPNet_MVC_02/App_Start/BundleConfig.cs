﻿using System.Web;
using System.Web.Optimization;

namespace LVL2_ASPNet_MVC_02
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {



            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/vendor/jquery/jquery.min.js",
                       "~/vendor/bootstrap/js/bootstrap.bundle.min.js",
                       "~/vendor/jquery-easing/jquery.easing.min.js",
                       "~/vendor/magnific-popup/jquery.magnific-popup.min.js",
                       "~/Scripts/creative.min.js"
                       ));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/vendor/fontawesome-free/css/all.min.css",
                      "~/vendor/magnific-popup/magnific-popup.css",
                      "~/Content/creative.min.css"));
        }
    }
}
